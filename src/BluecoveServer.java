import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.bluetooth.*;
import javax.microedition.io.*;

/**
 * This class starts an Bluetooth Server for serial port SPP
 * 
 * @author asteinbr
 *
 */
public class BluecoveServer {
	private static OutputStream outStream;
	public Thread recvT;
	
	// runServer
	public void runServer() throws IOException {
		LocalDevice localDevice = LocalDevice.getLocalDevice();
		System.out.println("Address: " + localDevice.getBluetoothAddress());
		System.out.println("Name: " + localDevice.getFriendlyName());

		// finally start the Server via startServer()
		this.startServer();

	} // runServer

	// startServer
	private void startServer() throws IOException {

		// Create a UUID
		UUID uuid = new UUID("1101", true); // serial, SPP
		// UUID uuid = new UUID("1105", true); // obex obj push
		// UUID uuid = new UUID("0003", true); // rfcomm
		// UUID uuid = new UUID("1106", true); // obex file transfer

		// Create the service url
		String connectionString = "btspp://localhost:" + uuid
				+ ";name=Sample SPP Server";

		// open server url
		StreamConnectionNotifier streamConnNotifier = (StreamConnectionNotifier) Connector
				.open(connectionString);

		// Wait for client connection
		System.out
				.println("\nServer Started. Waiting for clients to connect...");
		StreamConnection connection = streamConnNotifier.acceptAndOpen();

		// connect
		System.out.println("Connecting to client...");
		RemoteDevice dev = RemoteDevice.getRemoteDevice(connection);
		try {
			System.out.println("Remote device address: "
					+ dev.getBluetoothAddress());
			System.out.println("Remote device name: "
					+ dev.getFriendlyName(true));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		// read string from spp client
		recvT = new Thread(new recvLoop(connection));
		recvT.start();

		// send response to spp client
		Thread sendT = new Thread(new sendLoop(connection));
		sendT.start();

		System.out.println("\nServer threads started");

		// stay alive
		while (true) {
			try {
				Thread.sleep(2000);
				// System.out.println("\nServer looping.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	} // startServer

	// recvLoop
	private static class recvLoop implements Runnable {
		private StreamConnection connection = null;
		private InputStream inStream = null;
		private boolean STATE_OK = true;

		public recvLoop(StreamConnection c) {
			this.connection = c;
			try {
				this.inStream = this.connection.openInputStream();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			while (STATE_OK) {
				try {
					BufferedReader bReader = new BufferedReader(
							new InputStreamReader(inStream));
					String lineRead = bReader.readLine();
					System.out.println("Server recv: " + lineRead);
					try {
						if (lineRead.contains("010C")) {
							sendMessage(connection, "41 0C 0E A1  >");
						} else if (lineRead.contains("010D")) {
							sendMessage(connection, "41 0D 05  >");
						} else if (lineRead.contains("0105")) {
							sendMessage(connection, "41 05 3E  >");
						}
					}
					catch (NullPointerException npe) {
						System.out.println("An error occoured: " + npe.getStackTrace());
						STATE_OK = false;
					}
					Thread.sleep(200);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	} // recvLoop

	// sendLoop
	private static class sendLoop implements Runnable {
		private StreamConnection connection = null;
		PrintWriter pWriter = null;

		public sendLoop(StreamConnection c) {
			this.connection = c;
			// OutputStream outStream;
			try {
				outStream = this.connection.openOutputStream();
				this.pWriter = new PrintWriter(
						new OutputStreamWriter(outStream));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			while (true) {
				try {
					// prompt
					System.out.println("Enter message: ");
					BufferedReader bReader = new BufferedReader(
							new InputStreamReader(System.in));
					String inputline = bReader.readLine();
					pWriter.write(inputline);
					pWriter.flush();
					System.out.println("Server send: " + inputline);
					Thread.sleep(200);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	} // sendLoop

	// sendMessage
	private static void sendMessage(StreamConnection c, String response) {
		PrintWriter pWriter = null;

		pWriter = new PrintWriter(new OutputStreamWriter(outStream));

		pWriter.write(response);
		pWriter.flush();
		System.out.println(">> " + response);
	} // sendMessage

	// main
	public static void main(String[] args) {
		BluecoveServer bluecoveServer = new BluecoveServer();
		try {
			bluecoveServer.runServer();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} // main

} // class